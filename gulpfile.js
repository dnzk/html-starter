"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var path = require("path");
var pug = require("gulp-pug");
var connect = require("gulp-connect");
var autoprefixer = require("gulp-autoprefixer");

var styleSource = path.resolve(__dirname, "src", "styles", "**", "*.scss");
var styleDestination = path.resolve(__dirname, "build");
var templateSource = path.resolve(__dirname, "src", "**", "*.pug");
var templateDestination = path.resolve(__dirname, "build");
var styleBuildSource = path.resolve(__dirname, "build", "main.css");
var templateBuildSource = path.resolve(__dirname, "build", "index.html");

gulp.task("sass", function() {
  return gulp
    .src(styleSource)
    .pipe(
      sass({
        includePaths: require("node-normalize-scss").includePaths,
        outputStyle: "compressed"
      }).on("error", sass.logError)
    )
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions"]
      })
    )
    .pipe(gulp.dest(styleDestination));
});

gulp.task("watch:sass", function() {
  gulp.watch(styleSource, ["sass"]);
});

gulp.task("pug", function() {
  return gulp
    .src(templateSource)
    .pipe(
      pug({
        pretty: true
      })
    )
    .pipe(gulp.dest(templateDestination));
});

gulp.task("watch:pug", function() {
  gulp.watch(templateSource, ["pug"]);
});

gulp.task("connect", function() {
  connect.server({
    root: "build",
    port: 3001,
    livereload: true
  });
});

gulp.task("reload", function() {
  gulp.watch(templateBuildSource, function() {
    gulp.src(templateBuildSource).pipe(connect.reload());
  });
  gulp.watch(styleBuildSource, function() {
    gulp.src(styleBuildSource).pipe(connect.reload());
  });
});

gulp.task("default", ["watch:sass", "watch:pug", "connect", "reload"]);
